package com.crawler.wikipedia;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CrawlerRepository extends JpaRepository<CrawlerData, Long> {
}
