package com.crawler.wikipedia;

import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Component
public class CrawlerService {
    Logger logger = Logger.getLogger(CrawlerService.class);

    private static final String USER_AGENT =
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1";

    @Autowired
    CrawlerRepository crawlerRepository;

    public void crawlUrl(String url) {
        try {
            Connection connection = Jsoup.connect(url).userAgent(USER_AGENT);
            Document document = connection.get();
            Element header = document.select("#firstHeading").first();
            String name = header.text();
            Element logo = document.select("a.image").first();
            Element imageNode = logo.select("img").first();
            String logoPng = "https:" + imageNode.attr("src");

            //Open a URL Stream
            Connection.Response resultImageResponse = Jsoup.connect(logoPng)
                    .ignoreContentType(true).execute();

            File file = new File(name + "_" + "logo.png");
            FileOutputStream out = (new FileOutputStream(file));
            out.write(resultImageResponse.bodyAsBytes());  // resultImageResponse.body() is where the image's contents are.
            out.close();

            CrawlerData crawlerData = new CrawlerData();
            crawlerData.setName(name);
            crawlerData.setImgPath(file.getAbsolutePath());

            ExampleMatcher NAME_MATCHER = ExampleMatcher.matching()
                    .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.ignoreCase())
                    .withMatcher("imgPath",ExampleMatcher.GenericPropertyMatchers.ignoreCase());
            Example<CrawlerData> example = Example.of(crawlerData, NAME_MATCHER);
            if(!crawlerRepository.exists(example)) {
                crawlerRepository.save(crawlerData);
            }

        } catch (IOException ex) {
            logger.error("Unable to crawl site. IOException thrown: " + ex);
        }
    }
}