package com.crawler.wikipedia;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.apache.log4j.BasicConfigurator;
import org.apache.commons.validator.routines.UrlValidator;


@SpringBootApplication
public class CrawlerDemoApplication implements CommandLineRunner {

	@Autowired
	CrawlerService crawlerService;

	Logger logger = Logger.getLogger(CrawlerDemoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(CrawlerDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		BasicConfigurator.configure();
		if(UrlValidator.getInstance().isValid(args[0])) {
			crawlerService.crawlUrl(args[0]);
		}else{
			logger.error("Url not valid! Please input a valid Url and try again.");
		}
	}
}
