CREATE TABLE crawler_data (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(256) NOT NULL,
  img_path varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
)